# Papyrus Compiler Extension

Adds commands for running the [Papyrus compiler](https://www.creationkit.com/index.php?title=Category:Papyrus) from VS Code

Use the `Show All Commands` (default: Ctrl+Shift+P) and start typing 'Papyrus' (without quotes) to see the available commands.

Note: This is a preview release. Feedback is [welcome](https://www.creationkit.com/index.php?title=User:T-andersen)

## Features

* Command for compiling the edited file. 
* Command for compiling all scripts in source directory. 
* Command for compiling from a project file (.ppj). 
* Command for listing compiled scripts.
* Command for tailing the Papyurs log file. Including filtering log lines.

All compile commands supports the following stages

* Development: No optimisation.
* Release: Turns on release-mode processing, removing debug code from the script.
* Final:  Turns on final-mode processing, removing debug code and beta code from the script.

## Extension Settings

Please make sure the settings are correct before using this extension.

This extension contributes the following settings:

* `mrandersen.papyrus.compiler.path`: Full path to PapyrusCompiler.exe
* `mrandersen.papyrus.compiler.flags`: Full path to flag (.flg) file to use for processing flags in the scripts. You'll almost always want this to be the Institute_Papyrus_Flags.flg file
* `mrandersen.papyrus.compiler.import`: Specifies a list of directories that the compiler will look in for other scripts and the flag file. You'll want to at least point this at the directory that contains all the game's base scripts. Multiple folders are separated by semicolons.
* `mrandersen.papyrus.compiler.sourceDir`: Full path to the root directory of your source files.
* `mrandersen.papyrus.compiler.output`: Full path to the directory for the compiled files.
* `mrandersen.papyrus.compiler.projectFile`: (optional) Full path to project file (.ppj).
* `mrandersen.papyrus.compiler.logFile`: (optional) Full path to the log file.

## Known Issues

## Release Notes

See [CHANGELOG.md](https://gitlab.com/thomas.andersen/vsc-papyrus-compiler/blob/master/CHANGELOG.md) for details.

**Enjoy!**