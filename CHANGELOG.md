# Change Log
All notable changes to the "papyrus-compiler" extension will be documented in 
this file.

## 0.0.1
- Initial preview release.

## 0.1.0
- Add command to tail the log Papyurs log file.

## 0.1.1
- Version fixes.

## 0.1.2
- Added command for opening the log file in the editor.
- It is not possible to set a filter on the log file. Use 'Tail log file', press enter and add a string to filter on.
