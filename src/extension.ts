'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { setTimeout, setInterval } from 'timers';
import { ProcessExecution } from 'vscode';
import conf from './configuration';
import fileUtils from './fileUtils';
import compiledScripts from './compiledScripts';

interface ValidationResult {
    success: boolean;
    errors: Array<string>;
    scriptPath: string
}

enum Stage {
    Development = 1,
    Relase,
    Final
}

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
    
    /**
     * Compile file Development
     */
    context.subscriptions.push( vscode.commands.registerCommand('mrandersen.papyrus.compiler.compileCurrentFile', () => {
        validateAndCompileCurrentFile(Stage.Development);
    }));
    /**
     * Compile file Release
     */
    context.subscriptions.push(vscode.commands.registerCommand('mrandersen.papyrus.compiler.compileCurrentFileRelease', () => {
        validateAndCompileCurrentFile(Stage.Relase);
    }));
    /**
     * Compile file Final
     */
    context.subscriptions.push(vscode.commands.registerCommand('mrandersen.papyrus.compiler.compileCurrentFileFinal', () => {
        validateAndCompileCurrentFile(Stage.Final);
    }));

    /**
     * Compile All
     */
    context.subscriptions.push(vscode.commands.registerCommand('mrandersen.papyrus.compiler.compileAll', () => {
        validateAndcompileAll(Stage.Development);
    }));
    /**
     * Compile All Release
     */
    context.subscriptions.push(vscode.commands.registerCommand('mrandersen.papyrus.compiler.compileAllRelease', () => {
        validateAndcompileAll(Stage.Relase);
    }));
    /**
     * Compile All Final
     */
    context.subscriptions.push(vscode.commands.registerCommand('mrandersen.papyrus.compiler.compileAllFinal', () => {
        validateAndcompileAll(Stage.Final);
    }));

    /**
     * Compile Project File
     */
    context.subscriptions.push(vscode.commands.registerCommand('mrandersen.papyrus.compiler.compileProjectFile', () => {
        validateAndcompileProjectFile()
    }));

    /**
     * List Scripts
     */
    context.subscriptions.push(vscode.commands.registerCommand('mrandersen.papyrus.compiler.listCompiled', () => {
        const setting: vscode.Uri = vscode.Uri.parse("untitled:" + "C:\\Summary");
        vscode.workspace.openTextDocument(setting).then((a: vscode.TextDocument) => {
            vscode.window.showTextDocument(a, 1, false).then(e => {
                e.edit(edit => {
                    edit.insert(new vscode.Position(0, 0), compiledScripts.listCompiled());
                });
            });
        }, (error: any) => {
            console.error(error);
        });
    }));

    /**
     * Tail the log file
     */
    context.subscriptions.push( vscode.commands.registerCommand('mrandersen.papyrus.compiler.tailLogFile', () => {
        let selectedText:string = getActiveEditor().document.getText(getActiveEditor().selection);
        vscode.window.showInputBox({
            prompt: 'Only dispaly lines with this string. Use blank for no filter', 
            value: selectedText
        }).then(text => {
            if (text !== undefined) {
                tailLogFile(text);
            }
        });
    }));

    /**
     * Open the log file in editor
     */
    context.subscriptions.push( vscode.commands.registerCommand('mrandersen.papyrus.compiler.openLogFile', () => {
        vscode.workspace.openTextDocument(conf.getLogFile()).then((doc) => {
            vscode.window.showTextDocument(doc)
        }, (err) => console.error(err));
    })); 


    function validateAndCompileCurrentFile(stage: Stage = Stage.Development): void {
        let validated = validate();
        if (!validated.success) {
            displayErrors(validated.errors)
            return;
        }
        executeCompiler(validated.scriptPath, stage);
    }

    function validateAndcompileAll(stage: Stage = Stage.Development): void {
        let errors = validateConfiguration();
        if (errors) {
            displayErrors(errors)
            return;
        }
        executeCompiler(conf.getSourceDir(), stage);
    }

    function validateAndcompileProjectFile(stage: Stage = Stage.Development): void {
        let errors = validateProjectFile();
        if (errors) {
            displayErrors(errors);
            return;
        }
        executeCompiler(conf.getProjectFile())
    }

    function executeCompiler(pathToScripts: string, stage: Stage = Stage.Development): void {
        let stageFlags = resolveFlagsForStage(stage);
        let terminal = createTerminal();
        terminal.sendText(`& "${conf.getCompilerPath()}" "${pathToScripts}" -f="${conf.getFlags()}" -i="${conf.getImport()}" -o="${conf.getOutput()}" ${stageFlags}`);
        terminal.show(true);
    }

    function tailLogFile(filterText: string = ''): void {
        let terminal = createTerminal('Papyrus Log' + ((filterText !='') ? ` (Filter: ${filterText})` : ''));
        let logFile = conf.getConfig('mrandersen.papyrus.compiler.logFile').toString();

        let cmd = `& Get-Content -Path "${logFile}"`;
        cmd += ' -Wait';
        if ( filterText != '' ) {
            cmd += (filterText ? ` | Where-Object { $_.Contains("${filterText}") }` : '');
        }
        
        terminal.sendText(cmd);

        terminal.show(true);
    }

    function createTerminal(name = 'Papyrus Compiler'): vscode.Terminal {
        return vscode.window.createTerminal(name);
    }

    function getActiveEditor(): vscode.TextEditor {
        return vscode.window.activeTextEditor;
    }

    function resolveFlagsForStage(stage: Stage): string {
        switch(stage) {
            case Stage.Relase:
                return conf.getConfig('mrandersen.papyrus.compiler.releaseStageArgs').toString()
            case Stage.Final:
                return conf.getConfig('mrandersen.papyrus.compiler.finalStageArgs').toString()
            default:
                return conf.getConfig('mrandersen.papyrus.compiler.developmentStageArgs').toString()
        }
    }

    function validate(configurationOnly: boolean = false): ValidationResult {
        let result = {
            success: false,
            errors: [],
            scriptPath: null
        };

        let configurationErrors = validateConfiguration()
        if (configurationErrors.length > 0) {
            configurationErrors.forEach((error) => {
                result.errors.push(error);
            });
            result.success = false;
            result.errors = configurationErrors;
            return result;
        }
        
        let editor = getActiveEditor();
        if (!editor) {
            return result;
        }

        let document = editor.document;
        if (document.isDirty) {
            result.errors.push('Please save the file before compiling.')
            return result;
        }
        
        let scriptPath = document.uri.fsPath;
        if (!scriptPath) {
            return result;
        }

        if (!fileUtils.hasFileExtension(scriptPath,'psc')) {
            result.errors.push('File does not have a .psc extension.');
            return result;
        }

        result.success = true;
        result.scriptPath = scriptPath;
        return result;
    }

    function validateConfiguration(): string[] {
        let result = [];
        if (!conf.getCompilerPath()) {
            result.push('mrandersen.papyrus.compiler.path is not set.');
        } 
        if (!conf.getFlags()) {
            result.push('mrandersen.papyrus.compiler.flags is not set.');
        }
        if (!conf.getImport()) {
            result.push('mrandersen.papyrus.compiler.import is not set.');
        } 
        if (!conf.getOutput()) {
            result.push('mrandersen.papyrus.compiler.output is not set.');
        }

        return result;
    }

    function validateProjectFile(): string[] {
        let result = [];
        let compilerProjectFile = conf.getProjectFile();
        if (!compilerProjectFile || compilerProjectFile == '') {
            result.push('mrandersen.papyrus.compiler.projectFile is not set.');
        } 
        return result;
    }

    function displayErrors(errors) {
        errors.forEach((error) => {
            vscode.window.showErrorMessage('Papyrus Compiler: ' + error);
        });
    }
}

// this method is called when your extension is deactivated
export function deactivate() {
}