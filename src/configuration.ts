import * as vscode from 'vscode';

function getConfig(key: string) {
    return vscode.workspace.getConfiguration().get(key)
}

function getCompilerPath() {
    return getConfig('mrandersen.papyrus.compiler.path').toString();
}

function getFlags() {
    return getConfig('mrandersen.papyrus.compiler.flags').toString();
}

function getImport() {
    return getConfig('mrandersen.papyrus.compiler.import').toString();
}

function getOutput() {
    return getConfig('mrandersen.papyrus.compiler.output').toString();
}

function getSourceDir() {
    return getConfig('mrandersen.papyrus.compiler.sourceDir').toString();
}

function getProjectFile() {
    return getConfig('mrandersen.papyrus.compiler.projectFile').toString();
}

function getLogFile() {
    return getConfig('mrandersen.papyrus.compiler.logFile').toString();
}

export default {
    getConfig,
    getProjectFile,
    getSourceDir,
    getOutput,
    getImport,
    getFlags,
    getCompilerPath,
    getLogFile
}