'use strict';
exports.__esModule = true;
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
var vscode = require("vscode");
var path = require('path');
var fs = require('fs');
var walkSync = function (dir, filelist) {
    if (filelist === void 0) { filelist = []; }
    fs.readdirSync(dir).forEach(function (file) {
        filelist = fs.statSync(path.join(dir, file)).isDirectory()
            ? walkSync(path.join(dir, file), filelist)
            : filelist.concat(file);
    });
    return filelist;
};
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
function activate(context) {
    var gameDir = vscode.workspace.getConfiguration().get('mrandersen.papyrus.game.dir');
    var compilerPath = vscode.workspace.getConfiguration().get('mrandersen.papyrus.compiler.path');
    var compilerFlagsArg = vscode.workspace.getConfiguration().get('mrandersen.papyrus.compiler.flags');
    var compilerImportArg = vscode.workspace.getConfiguration().get('mrandersen.papyrus.compiler.import');
    var compilerImportOutputArg = vscode.workspace.getConfiguration().get('mrandersen.papyrus.compiler.output');
    context.subscriptions.push(vscode.commands.registerCommand('mrandersen.papyrus.compiler.compileFile', function () {
        var configurationErrors = validateConfiguration();
        if (configurationErrors.length > 0) {
            configurationErrors.forEach(function (error) {
                vscode.window.showInformationMessage('Papyrus compiler: ' + error);
            });
            return;
        }
        var editor = getActiveEditor();
        if (!editor) {
            return;
        }
        var document = editor.document;
        if (document.isDirty) {
            vscode.window.showInformationMessage('Please save the file before compiling.');
            return;
        }
        var pathToScript = document.uri.fsPath;
        if (!pathToScript) {
            return;
        }
        if (!isPapyrusScript(pathToScript)) {
            return;
        }
        executeCompiler(pathToScript);
    }));
    context.subscriptions.push(vscode.commands.registerCommand('mrandersen.papyrus.compiler.showCompiled', function () {
        var sourceDir = compilerImportOutputArg + '\\Source\\User';
        var files = walkSync(sourceDir);
        var longest = files.reduce(function (a, b) { return a.length > b.length ? a : b; });
        var result = [];
        result.push('File'.padEnd(longest.length) + '\tCompiled');
        files.forEach(function (file, index) {
            var compiledFilePath = compilerImportOutputArg + '\\' + file.replace('.psc', '.pex');
            result.push(file.padEnd(longest.length) + '\t' + fs.existsSync(compiledFilePath));
        });
        var setting = vscode.Uri.parse("untitled:" + "C:\\Summary");
        vscode.workspace.openTextDocument(setting).then(function (a) {
            vscode.window.showTextDocument(a, 1, false).then(function (e) {
                e.edit(function (edit) {
                    edit.insert(new vscode.Position(0, 0), result.join('\n'));
                });
            });
        }, function (error) {
            console.error(error);
        });
    }));
    function executeCompiler(pathToScript) {
        var terminal = createTerminal();
        terminal.sendText("& \"" + compilerPath + "\" \"" + pathToScript + "\" -f=\"" + compilerFlagsArg + "\" -i=\"" + compilerImportArg + "\" -o=\"" + compilerImportOutputArg + "\"; echo \"\nCompiled to: " + compilerImportOutputArg + "\\" + path.basename(pathToScript) + "\" ");
        terminal.show(true);
    }
    function createTerminal() {
        return vscode.window.createTerminal("Papyrus Compiler");
    }
    function getActiveEditor() {
        return vscode.window.activeTextEditor;
    }
    function validateConfiguration() {
        var result = [];
        if (!gameDir) {
            result.push('mrandersen.papyrus.game.dir is not set.');
        }
        if (!compilerPath) {
            result.push('mrandersen.papyrus.compiler.path is not set.');
        }
        if (!compilerFlagsArg) {
            result.push('mrandersen.papyrus.compiler.flags is not set.');
        }
        if (!compilerImportArg) {
            result.push('mrandersen.papyrus.compiler.import is not set.');
        }
        if (!compilerImportOutputArg) {
            result.push('mrandersen.papyrus.compiler.output is not set.');
        }
        return result;
    }
    function isPapyrusScript(fileName) {
        return path.extname(fileName) == '.psc';
    }
}
exports.activate = activate;
// this method is called when your extension is deactivated
function deactivate() {
}
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map