
const path = require('path')
const fs = require('fs');

function hasFileExtension(fileName: string, fileExtension: string): boolean {
    return path.extname(fileName) == '.' + fileExtension;
}

function fileExists(path: string): boolean {
    return fs.existsSync(path);
}

const walkSync = (dir, filelist = [], baseDir) => {
    fs.readdirSync(dir).forEach(file => {
        let fullPath = path.join(dir,file);
        let isDir = fs.statSync(path.join(dir, file)).isDirectory();
            filelist = isDir 
                ? walkSync(fullPath, filelist, baseDir) 
                : filelist.concat({
                    fullPath: fullPath,
                    localPath: fullPath.replace(baseDir, ''),
                    fileName: file
                });
    });
    return filelist;
}

function getBaseName(filePath: string): string {
    return path.basename(filePath)
}

function getModifiedDate(filePath: string): Date {
    return fs.statSync(filePath).mtime;
}

function getFileSizeInBytes(path:string) {
    const stats = fs.statSync(path)
    const fileSizeInBytes = stats.size
    return fileSizeInBytes
}

export default {
    hasFileExtension,
    walkSync,
    getBaseName,
    fileExists,
    getModifiedDate,
    getFileSizeInBytes
}