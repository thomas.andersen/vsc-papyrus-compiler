import * as vscode from 'vscode';
import conf from './configuration';
import fileUtils from './fileUtils';

function listCompiled() {
    let result = [];
    let outPutDir = conf.getOutput();
    let sourceDir = conf.getSourceDir();
    let compiledFiles = fileUtils.walkSync(outPutDir, [], outPutDir);
    result.push('Has Source\tCompiled Date           \tFile');
    result.push('-------------------------------------------------------------------------------');
    compiledFiles.forEach((compiledFile, index) => {
        if (!compiledFile.localPath.match(/^\\Source|^\\DLC05|^\\Hardcore/)) {
            let sourceFile = sourceDir + compiledFile.localPath.replace('.pex','.psc');
            let exists = fileUtils.fileExists(sourceFile);
            result.push((exists ? '    Yes   ':'    No    ') + '\t' + fileUtils.getModifiedDate(compiledFile.fullPath).toISOString() + '\t' + compiledFile.localPath);
        }
    });
    return result.join('\n');
}

export default {
    listCompiled
}